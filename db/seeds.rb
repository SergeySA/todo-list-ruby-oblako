# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

seed_file = Rails.root.join('db', 'seeds', 'seeds.yml')
config = HashWithIndifferentAccess.new(YAML::load_file(seed_file))

# Iterate for each projects list
config['projects'].each do |project|
	# Create new project with title taken from first record in hash, store corrent project in variable
	current_project = Project.create(Hash[*project.first])
	# for each todo in current project
	project['todos'].each do |todo|
		# Store new todo in variable
		# Since yaml doesnt have current project id, add "project_id" => id hash to hashlist
		new_todo = todo.merge({"project_id" => current_project[:id]})
		# Call create method
		Todo.create(new_todo)
	end
end