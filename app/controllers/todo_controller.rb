class TodoController < ApplicationController
	def index
	    @projects = Project.all
	    @todos = Todo.all
	end

	def new 
		@todo = Todo.new
	end

	def create 
		@received_object_id = params[:todo][:project_id]
		
		@todo = Todo.new(todo_params)
		
		if(@todo.save)
            #Saved successfully; go to the class index
            redirect_to root_path 
        else
        	@todos = Todo.all
            #Saving failed; show the "create" form again...
            render root_path 
        end	
	end

	def update
		
	end

	def todo_params
		params.require(:todo).permit(:text, :isCompleted, :project_id)
	end

end
